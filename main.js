function Hamburger(size, stuffing) {
  try {
    if (arguments.length !== 2) {
      throw new HamburgerException("Здесь должно быть два аргумента!");
    }
    if (size !== Hamburger.SIZE_SMALL && size !== Hamburger.SIZE_LARGE) {
      throw new HamburgerException("Размер гамбургера указан неверно!");
    }
    if (
      stuffing !== Hamburger.STUFFING_CHEESE &&
      stuffing !== Hamburger.STUFFING_SALAD &&
      stuffing !== Hamburger.STUFFING_POTATO
    ) {
      throw new HamburgerException("Выберите начинку для бургера!");
    }

    this.size = size;
    this.stuffing = stuffing;
    this.toppings = [];
  } catch (e) {
    console.log(e);
  }
}

Hamburger.SIZE_SMALL = { size: "small", price: 50, calories: 20 };
Hamburger.SIZE_LARGE = { size: "large", price: 100, calories: 40 };
Hamburger.STUFFING_CHEESE = { stuffing: "cheese", price: 10, calories: 20 };
Hamburger.STUFFING_SALAD = { stuffing: "salad", price: 20, calories: 5 };
Hamburger.STUFFING_POTATO = { stuffing: "potato", price: 15, calories: 10 };
Hamburger.TOPPING_MAYO = { topping: "mayo", price: 20, calories: 5 };
Hamburger.TOPPING_SPICE = { topping: "spice", price: 15, calories: 0 };

Hamburger.prototype.addTopping = function() {
  try {
    if (arguments.length === 0) {
      throw new HamburgerException("Нужно передать хотя бы 1 топпинг!");
    }
    if (
      topping !== Hamburger.TOPPING_MAYO &&
      topping !== Hamburger.TOPPING_SPICE
    ) {
      throw new HamburgerException("Выберите топпинг для гамбургера!");
    }

    for (var i = 0; i < arguments.length; i++) {
      this.toppings.push(arguments[i]);
    }
  } catch (e) {
    console.log(e);
  }
};

Hamburger.prototype.removeTopping = function(topping) {
  try {
    if (arguments.length !== 1) {
      throw new HamburgerException("Введите топпинг для удаления");
    }
    toppingIndex = this.toppings.indexOf(topping);
    this.toppings.splice(toppingIndex, 1);
  } catch (e) {
    console.log(e);
  }
};

Hamburger.prototype.getToppings = function() {
  return this.toppings;
};

Hamburger.prototype.getSize = function() {
  return this.size.size;
};

Hamburger.prototype.getStuffing = function() {
  return this.stuffing.stuffing;
};

Hamburger.prototype.calculatePrice = function() {
  const toppingsPrice = this.toppings.reduce(
    (total, topping) => total + topping.price,
    0
  );
  return this.size.price + toppingsPrice + this.stuffing.price;
};

Hamburger.prototype.calculateCalories = function() {
  const toppingsCalories = this.toppings.reduce(
    (total, topping) => total + topping.calories,
    0
  );
  return this.size.calories + toppingsCalories + this.stuffing.calories;
};

function HamburgerException(message) {
  this.name = "Hamburger Exception";
  this.message = message;
}

HamburgerException.prototype = Object.create(Error.prototype);

// ============================

var h = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);

// h.addTopping(Hamburger.TOPPING_MAYO, Hamburger.TOPPING_SPICE);

console.log(h);
